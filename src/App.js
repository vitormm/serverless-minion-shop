import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Segment, Menu, Icon } from 'semantic-ui-react'
import CreateMinion from './screens/createMinion'
import Amplify, { API } from 'aws-amplify';
import aws_exports from './aws-exports';
Amplify.configure(aws_exports);

class App extends Component {

  render() {
    return (
      <Segment>
        <Menu>
           <Menu.Item name='home'> <Icon name="shop"/></Menu.Item>
           <Menu.Item name='Minions'/>
           <Menu.Item name='aboutUs' />
         </Menu>
         <CreateMinion />
      </Segment>
    );
  }
}

export default App;
