import React, { Component } from 'react';
import { Form, Button, Container } from 'semantic-ui-react'

import Amplify, { API } from 'aws-amplify';

const uuidv1 = require('uuid/v1');

class CreateMinion extends Component {


constructor(props) {
    super(props)
    this.state = {}
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
handleChange(event, {name, value}) {
    this.setState({ [name]: value });
  }

handleSubmit(event) {
  let apiName = 'ServerlessReactExampleCRUD';
  let path = '/ServerlessReactExample';
  let newItem = {
    body: {
        ID: uuidv1(), UserEmail: this.state.userEmail, UserName: this.state.userName, MinionQtd: this.state.minionQtd
      }
    }
  API.post(apiName, path, newItem).then(response => {
    console.log(response)
  }).catch(error => {
    console.log(error.response)
  });
  event.preventDefault();
  // this.handleClose()
}

// handleOpen = () => this.setState({ modalOpen: true })
// handleClose = () => this.setState({ modalOpen: false })
render () {
    return (
        <Form onSubmit={this.handleSubmit}>
          <Form.Group unstackable widths={2}>
            <Form.Input name='userEmail' label='Email' placeholder='Enter your email...' onChange={this.handleChange} value={this.state.userEmail} />
            <Form.Input name='userName' label='Name' placeholder='Enter your name...' onChange={this.handleChange} value={this.state.userNamel} />
            <Form.Input name='minionQtd' label='Quantity' placeholder='Quantity...' onChange={this.handleChange} value={this.state.minionQtd} type='number' />
          </Form.Group>
          
<Form.Button type='submit'>Submit</Form.Button>
        </Form>
      );
    }
  }
export default CreateMinion;